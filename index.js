const AWS = require('aws-sdk');
const jmespath = require('jmespath');
const fse = require('fs-extra');
const awsQueries = require('./aws-queries');
// const awsQueries = require('./aws-queries-select');

const outputDir = 'output';

const selectedRegions = ['us-east-1'];

async function getRegions(override = []) {
  const globalPlus = (regionList = []) => ['global'].concat(regionList);

  if (override.length) {
    return globalPlus(override);
  }

  const ec2 = new AWS.EC2({
    region: process.env.AWS_REGION || 'us-east-1',
  });

  return ec2
    .describeRegions()
    .promise()
    .then((data) => {
      const regions = data.Regions.map((r) => r.RegionName).sort();
      return globalPlus(regions);
    });
}

function createTsvHead(headings) {
  let tsvHead = '#\t';
  for (let h = 0; h < headings.length; h++) {
    if (typeof headings[h] === 'string') {
      tsvHead += headings[h];
    } else {
      tsvHead += Object.keys(headings[h])[0];
    }
    if (h < headings.length - 1) tsvHead += '\t';
  }
  tsvHead += '\n';
  return tsvHead;
}

function createTsvBody(headings, data) {
  const rows = [];
  for (let d = 0; d < data.length; d++) {
    let tsvRow = `${d + 1}\t`;
    for (let h = 0; h < headings.length; h++) {
      if (typeof headings[h] === 'string') {
        try {
          tsvRow += jmespath.search(data[d], headings[h]);
        } catch (e) {
          console.error(`Error with jmespath: ${headings[h]}: ${e.message}`);
          console.error(e);
        }
      } else {
        try {
          tsvRow += jmespath.search(data[d], Object.values(headings[h])[0]);
        } catch (e) {
          // weird prettier formatting issue
          // eslint-disable-next-line
          console.error(`Error with jmespath: ${Object.values(headings[h])[0]}: ${e.message}`);
          console.error(e);
        }
      }
      if (h < headings.length - 1) tsvRow += '\t';
    }
    rows.push(tsvRow);
  }
  return rows.join('\n');
}

function getInventory(regions = []) {
  console.log('Generating inventory...\n');

  // Output results to ./<output-dir>
  fse.ensureDirSync(outputDir);

  for (let r = 0; r < regions.length; r++) {
    const region = regions[r];
    // separate outputs per region ./<output-dir>/<region>
    fse.ensureDirSync(`${outputDir}/${region}`);
    for (let q = 0; q < awsQueries.length; q++) {
      const query = awsQueries[q];
      if (
        region === 'global' &&
        (!query.hasOwnProperty('region') || query.region !== 'global')
      ) {
        continue; // show only global resources on global output
      }
      if (
        region !== 'global' &&
        query.hasOwnProperty('region') &&
        query.region === 'global'
      ) {
        continue; // do not show global resources in other regions
      }
      if (query.hasOwnProperty('region') && query.region !== region) {
        continue; // restrict to specific region only
      }
      if (
        query.hasOwnProperty('skipRegions') &&
        query.skipRegions.includes(region)
      ) {
        continue; // skip specified regions
      }
      let caller;
      try {
        caller = new AWS[query.service]({ region });
      } catch (e) {
        console.error(
          `Error using query.service ${query.service}: `,
          e.message
        );
        continue;
      }

      const handler = (err, data) => {
        if (err) {
          console.error(err);
          return;
        }

        let information;

        try {
          information = jmespath.search(data, `${query.jmespath}[]`);
        } catch (e) {
          console.error(
            `Error with jmespath: ${query.jmespath}[]: ${e.message}`
          );
          console.error(e);
        }

        if (information && information.length > 0) {
          let tsv = createTsvHead(query.headings);
          tsv += createTsvBody(query.headings, information);
          const fileName = `${outputDir}/${region}/${query.id}.tsv`;
          fse.writeFileSync(fileName, tsv);
          console.log(fileName);
        }
      };

      try {
        if (query.hasOwnProperty('params')) {
          caller[query.api].call(caller, query.params, handler);
        } else {
          caller[query.api].call(caller, handler);
        }
      } catch (e) {
        console.error(
          `Error calling ${query.service}.${query.api}: ${e.message}`
        );
        throw e;
      }
    }
  }
}

getRegions(selectedRegions).then(getInventory).catch(console.error);

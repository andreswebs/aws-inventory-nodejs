const awsQueries = [
  {
    service: 'SQS',
    api: 'listQueues',
    title: 'Simple Queue Service (SQS) Queues',
    id: 'sqs-queues',
    jmespath: 'map(&{URL:@}, QueueUrls)',
    headings: ['URL'],
  },
];

module.exports = awsQueries;
